<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Meteor HTML CSS Template</title>
<!--

Template 2089 Meteor

http://www.tooplate.com/view/2089-meteor

-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="{{asset('admin/css/sb-admin-2.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/sb-admin-2.min.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/fontAwesome.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/hero-slider.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/tooplate-style.css')}}">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <script src="{{asset('main/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
    </head>

<body style="background: white;">
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
  <a  href="{{ url('/home') }}"><button type="button" class="btn btn-outline-info navbar-btn">Home</button></a>
  </div>
</nav>
    <!--/.header-->
    <div class="container" style="margin-top: 50px">

    @yield('content')
    
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="public/main/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="{{asset('main/js/vendor/bootstrap.min.js')}}"></script>

    <script src="{{asset('main/js/plugins.js')}}"></script>
    <script src="{{asset('main/js/main.js')}}"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    </script>
</body>
</html>