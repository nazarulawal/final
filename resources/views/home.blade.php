<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Meteor HTML CSS Template</title>
<!--

Template 2089 Meteor

http://www.tooplate.com/view/2089-meteor

-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="{{asset('main/css/bootstrap-theme.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/fontAwesome.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/hero-slider.css')}}">
        <link rel="stylesheet" href="{{asset('main/css/tooplate-style.css')}}">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <script src="{{asset('main/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>

       
    </head>
    <body>
    <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <ul class="nav justify-content-end">
                    <li class="nav-item">
                    <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                    </li>

                    </ul>
                </div>
            </div>
        </nav>

<div id="about" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h4>Main Bareng</h4>
                        <div class="line-dec"></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <a href="/admin/basket/create">
                <div class="col-md-3 col-sm-6 col-xs-12" style="margin-left: 120px">
                    <div class="service-item first-service">
                        <div class="icon"></div>
                        <h4>Basket Ball</h4>
                        <p>Yuk Gabung Bersama Kambi Bermain Basket Bersama Untuk Melawan Kegabutan.</p>
                    </div>
                </div>
                </a>
                <a href="/admin/futsal/create">
                 <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="service-item second-service">
                        <div class="icon"></div>
                        <h4>Futsal</h4>
                        <p>Daripada gabut dikosan mending gabung bareng kami bermain futsal bersama orang orang baru dan teman baru</p>
                    </div>
                </div>
                </a>
                <a href="/admin/volley/create">
                 <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="service-item third-service">
                        <div class="icon"></div>
                        <h4>Volly</h4>
                        <p>Bermain volly bersama orang baru dan teman baru, yuk segera join!</p>
                    </div>
                </div>
                </a>
            </div>
        </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="public/main/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="{{asset('main/js/vendor/bootstrap.min.js')}}"></script>

    <script src="{{asset('main/js/plugins.js')}}"></script>
    <script src="{{asset('main/js/main.js')}}"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    </script>
</body>
</html>