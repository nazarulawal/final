@extends('layouts.isi')

@section('content')
<form action="/admin/volley" method="POST">
{{csrf_field()}} 
    <h3>Join Main Futsal</h3>
    <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" class="form-control" id="text" placeholder="Nama">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Posisi di Volley</label>
        <input type="text" class="form-control" id="text" placeholder="Posisi Saat Bermain">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Alamat</label>
        <input type="text" class="form-control" id="text" placeholder="Alamat">
    </div>
    <!-- Single button -->
    <div class="form-group">
    <label for="exampleFormControlSelect1">Pilih Tempat Bermain</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Dago</option>
      <option>Pasteur</option>
      <option>Cibiru</option>
      <option>Lembang</option>
    </select>
  </div>
    
    <div class="form-group">
    <label for="exampleFormControlSelect1">Pilih Waktu Bermain</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Pukul 10.00 WIB</option>
      <option>Pukul 14.00 WIB</option>
      <option>Pukul 16.00 WIB</option>
      <option>Pukul 19.00 WIB</option>
    </select>
  </div>

  <button type="submit" class="btn btn-success btn-lg">Submit</button>
    
</form>
@endsection